from flask import Flask
from flask import request, make_response
from flask import render_template
from flask import url_for
import pandas as pd
import sqlite3

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/contacts')
def contacts():
    return render_template('contacts.html')


@app.route('/test', methods=['GET', 'POST'])
def test(res=None):
    if request.method == 'POST':
        df = pd.read_csv('TTT.csv')

        Pl, Gor, Exc, Ext, Sh, Spa, Sem = 2, 2, 2, 2, 2, 2, 2
        if request.form.get('type'):
            Pl, Gor, Exc, Ext, Sh, Spa, Sem = 1, 1, 1, 1, 1, 1, 1
        else:
            if request.form.get('Pl'):
                Pl = int(request.form.get('Pl'))
            if request.form.get('Gor'):
                Gor = int(request.form.get('Gor'))
            if request.form.get('Exc'):
                Exc = int(request.form.get('Exc'))
            if request.form.get('Ext'):
                Ext = int(request.form.get('Ext'))
            if request.form.get('Sh'):
                Sh = int(request.form.get('Sh'))
            if request.form.get('Spa'):
                Spa = int(request.form.get('Spa'))
            if request.form.get('Sem'):
                Sem = int(request.form.get('Sem'))

        Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec = 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
        if request.form.get('time'):
            Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec = 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
        else:
            if request.form.get('Jan'):
                Jan = int(request.form.get('Jan'))
            if request.form.get('Feb'):
                Feb = int(request.form.get('Feb'))
            if request.form.get('Mar'):
                Mar = int(request.form.get('Mar'))
            if request.form.get('Apr'):
                Apr = int(request.form.get('Apr'))
            if request.form.get('May'):
                May = int(request.form.get('May'))
            if request.form.get('Jun'):
                Jun = int(request.form.get('Jun'))
            if request.form.get('Jul'):
                Jul = int(request.form.get('Jul'))
            if request.form.get('Aug'):
                Aug = int(request.form.get('Aug'))
            if request.form.get('Sep'):
                Sep = int(request.form.get('Sep'))
            if request.form.get('Oct'):
                Oct = int(request.form.get('Oct'))
            if request.form.get('Nov'):
                Nov = int(request.form.get('Nov'))
            if request.form.get('Dec'):
                Dec = int(request.form.get('Dec'))

        Eu, As, NA, SA, AO, Af = 2, 2, 2, 2, 2, 2
        if request.form.get('zone'):
            Eu, As, NA, SA, AO, Af = 1, 1, 1, 1, 1, 1
        else:
            if request.form.get('Eu'):
                Eu = int(request.form.get('Eu'))
            if request.form.get('As'):
                As = int(request.form.get('As'))
            if request.form.get('NA'):
                NA = int(request.form.get('NA'))
            if request.form.get('SA'):
                SA = int(request.form.get('SA'))
            if request.form.get('AO'):
                AO = int(request.form.get('AO'))
            if request.form.get('Af'):
                Af = int(request.form.get('Af'))

        df = df[(df.Пляжный == Pl) | (df.Горнолыжный == Gor) | (df.Экскурсионный == Exc) | (df.Экстремальный == Ext) | (
                    df.Шоппинг == Sh) | (df.СПА == Spa) | (df.Семейный == Sem)]

        if request.form.get('price'):
            if str(request.form.get('price')) != 'Не важно':
                price = request.form.get('price')
                df = df[df.Стоимость == price]

        if request.form.get('visa'):
            if (int(request.form.get('visa')) == 1) | (int(request.form.get('visa')) == 0):
                visa = request.form.get('visa')
                df = df[df.Виза == visa]

        df = df[(df.Январь == Jan) | (df.Февраль == Feb) | (df.Март == Mar) | (df.Апрель == Apr) | (df.Май == May) | (
                    df.Июнь == Jun) | (df.Июль == Jul) | (df.Август == Aug) | (df.Сентябрь == Sep) | (
                            df.Октябрь == Oct) | (df.Ноябрь == Nov) | (df.Декабрь == Dec)]

        df = df[(df.Европа == Eu) | (df.Азия == As) | (df.СевернаяАмерика == NA) | (df.ЮжнаяАмерика == SA) | (
                    df.АвстралияиОкеания == AO) | (df.Африка == Af)]

        if len(df.index) != 0:
            res = df.Страна.unique()
            return render_template('vivod.html', res=res)

        else:
            res = 'Нет такой страны, попробуйте снова'
            return render_template('error_vivod.html', res=res)

    return render_template('test.html')


@app.route('/<string:count>', methods=['GET', 'POST'])
def country(count):
    conn = sqlite3.connect("descrip.db")
    cursor = conn.cursor()
    l = cursor.execute(
        """
     SELECT 
         Описание
     FROM 
         Descrip 
     WHERE
         Страна=""" + "'" + count + "'" + """ 
       """).fetchall()
    description = l[0]
    i = cursor.execute(
        """
     SELECT 
         Изображение
     FROM 
         Descrip 
     WHERE
         Страна=""" + "'" + count + "'" + """ 
       """).fetchall()
    img = i[0]
    return render_template('country.html', description=description[0], image=img[0], count=count)


app.run()